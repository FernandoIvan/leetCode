// leetcode #1
// https://leetcode.com/problems/two-sum/description/

const twoSum = (numbers, target) => {
  const sums = [];
  // number we are using to compare, i
  for (let i = 0; i < numbers.length; i++) {
    // number we are comparing to, j
    for (let j = i + 1; j < numbers.length; j++) {
      if (numbers[i] + numbers[j] == target) {
        sums.push([i, j]);
      }
    }
  }
  // converting to single dimensional array
  return sums.reduce(prev => {
    return prev;
  });
};

// runtime
const numbers = [2, 7, 11, 15];
const target = 9;
twoSum(numbers, target); //> [0,1]
