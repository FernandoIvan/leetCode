# 1 - Two Sum

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

- Example

```javascript
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
```

---

## V1 Issue

### Issue #1

Comparing a number with it's neighbor, we always want to compare a number at `index` with it's neighbor, `index+1`

```javascript
for (let i = 0; i < numbers.length; i++) {
  for (let j = i + 1; j < numbers.length; j++) {
    if (numbers[i] + numbers[j] == target) {
      sums.push([i, j]);
    }
  }
}
```

### Issue #2

Question is looking for a one-dimensional array,
pushing an array into an array gives you a nested array.

```javascript
const sums = [];
sums.push([0, 1]);
return sums; //> [[0,1]]
```

**Solution** We need to convert or flatten the array into one dimension.

```javascript
const array = [[0, 1]];
array.reduce(array => {
  return array;
});
return array; //> [0,1]
```

---

## Full Code

```javascript
const twoSum = (numbers, target) => {
  const sums = [];
  // number we are using to compare, i
  for (let i = 0; i < numbers.length; i++) {
    // number we are comparing to, j
    for (let j = i + 1; j < numbers.length; j++) {
      if (numbers[i] + numbers[j] == target) {
        sums.push([i, j]);
      }
    }
  }
  // converting to single dimensional array
  return sums.reduce(prev => {
    return prev;
  });
};
```
