class Solution:
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        # convert every number to string
        numbers_string = [str(x) for x in digits]
        # concat and add a number
        numbers = int(''.join(numbers_string)) + 1
        # convert into an array of integers
        numbers_array = [int(number) for number in str(numbers)]
        return numbers_array

digits = [1,2,10]

solution = Solution()
print(solution.plusOne(digits)) #> [1,2,1,1]
