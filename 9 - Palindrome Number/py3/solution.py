class Solution:
    def isPalindrome(self, x):
        original_str = str(x).lower()
        reversed_str = "".join(reversed(original_str))
        if original_str == reversed_str:
            return True
        return False

solution = Solution()

# test cases
print(solution.isPalindrome("aPPle")) # > False
print(solution.isPalindrome("ana")) # > True
print(solution.isPalindrome("anA")) # > True
