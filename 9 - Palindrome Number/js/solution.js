const isPalindrome = function(x) {
  const original = String(x).toLowerCase();
  const reversed = original.split("").reverse().join("");
  if(original == reversed){return true}
  return false;
};

console.log(isPalindrome("apple")); //false
console.log(isPalindrome("pooP")); //true