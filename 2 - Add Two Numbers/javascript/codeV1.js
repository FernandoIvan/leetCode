// leetcode #2
// https://leetcode.com/problems/add-two-numbers/description/

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * 
/* 
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

//  input
// [2,4,3]
// [5,6,4]

// LinkedList interpretation
/*
const l1 = {
  ListNode{
    value:2,
    next: ListNode{
      value: 4,
      next: ListNode{
        value: 3,
        next: null
      }
    }
  }
} 
*/

function ListNode(val){
  this.val = val;
  this.next = null;
}

var addTwoNumbers = function(l1, l2) {
};

addTwoNumbers()
