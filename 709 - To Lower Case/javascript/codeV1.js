// leetcode #709
// https://leetcode.com/problems/to-lower-case/description/

const toLowerCase = function(str){
  return str.toLowerCase();
}
