// leetcode #804
// https://leetcode.com/problems/unique-morse-code-words/description/
// TODO replace functions using sets and maps. Branch 802_revision_1 is the example

var uniqueMorseRepresentations = function(words) {
  morseCode = []
  morsePair = createPair();
  words.forEach((word)=>{
      morseWord = word.split("").reduce((total,letter)=>{
          total += morsePair[letter]
          return total
      },"");
      morseCode.push(morseWord)
  });
  return morseCode.filter(uniq).length
};

const createPair = () =>{
keyPair = {};
morseCodes = [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."];
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
morseCodes.forEach((code, m_index)=>{
    alphabet.forEach((letter, a_index)=>{
        if(m_index == a_index){
            keyPair[letter] = code
        }
    });
});
return keyPair
}

const uniq = (value, index, self) =>{
  return self.indexOf(value) == index
}

console.log(uniqueMorseRepresentations(["gin", "zen", "gig", "msg"]));
