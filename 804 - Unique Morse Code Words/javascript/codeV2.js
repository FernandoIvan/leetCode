// leetcode #804
// https://leetcode.com/problems/unique-morse-code-words/description/

var uniqueMorseRepresentations = function(words) {
  const transformation = new Set();
  console.log(transformation)
  words.forEach((word)=>{
      transformation.add(word.split("").map(letter => getPair().get(letter)).join(''))
  });
  return transformation.size
};

const getPair = () =>{
morseCodes = [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."];
alphabet = 'abcdefghijklmnopqrstuvwxyz';
morsePair = new Map( morseCodes.map((code, index) => [alphabet[index], code]) );
return morsePair
}

const uniq = (value, index, self) =>{
return self.indexOf(value) == index
}

console.log(uniqueMorseRepresentations(["rwjje","aittjje","auyyn","lqtktn","lmjwn"]))