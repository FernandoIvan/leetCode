// leetcode #461
// https://leetcode.com/problems/hamming-distance/description/

const hammingDistance = function(x, y) {
  let xInBinary = x.toString(2);
  let yInBinary = y.toString(2);

  [xInBinary, yInBinary] = normalize(xInBinary, yInBinary);
  let distance = 0;

  xInBinary.split("").forEach((xBit, xIndex) => {
    yInBinary.split("").forEach((yBit, yIndex) => {
      if (xBit != yBit && xIndex == yIndex) {
        distance++;
      }
    });
  });

  return distance;
};

const normalize = function(x, y) {
  if (x.length < y.length) {
    for (let i = x.length; i < y.length; i++) {
      x = "0" + x;
    }
  } else {
    for (let i = y.length; i < x.length; i++) {
      y = "0" + y;
    }
  }
  return [x, y];
};

console.log(hammingDistance(1, 4));
