# 461 - Hamming Distance

The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

Given two integers x and y, calculate the Hamming distance.

Note:
0 ≤ x, y < 231.

```javascript
Input: x = 1, y = 4

Output: 2

Explanation:
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑

//The above arrows point to positions where the corresponding bits are different.
```

---

## What is Hamming Distance

Hamming Distance is number of mismatches at the same position.

---

## V1 Issues

### Issue #1 : Figuring out how to convert a Number to binary.

=> JavaScript's toString() function accepts arguments that will return that base value.
Example:

```javascript
const oneHundred = 100;
oneHundred.toString(2); //1100100
```

### Issue #2 : Binary numbers need to be the same length

=> My approach is to prepend '0's to the smaller binary string, I called this my normalize function.

```javascript
const normalize = function(x, y) {
  if (x.length < y.length) {
    for (let i = x.length; i < y.length; i++) {
      x = "0" + x;
    }
  } else {
    for (let i = y.length; i < x.length; i++) {
      y = "0" + y;
    }
  }
  return [x, y];
};
```

I'm returning an array with both values to deconstruct later in my main function.

### Issue #3: I need to assign both returned values in my main function.

=> Solution? Destructuring assignments

```javascript
[a, b] = [10, 20];
console.log(a); //10
console.log(b); //20
```

Using this I assigned both returned variables to multiple variables in my main function.

```javascript
let xInBinary = x.toString(2);
let yInBinary = y.toString(2);
[xInBinary, yInBinary] = normalize(xInBinary, yInBinary);
```

### Issue #4 My iterator is incrementing too many times

The code block:

```javascript
xInBinary.split("").forEach(xBit => {
  yInBinary.split("").forEach(yBit => {
    if (xBit != yBit) {
      distance++;
    }
  });
});
```

For some inputs iterated through the same pair multiple times.

=> Solution? Match against index too, since we only want to compare them at the same location.

```javascript
xInBinary.split("").forEach((xBit, xIndex) => {
  yInBinary.split("").forEach((yBit, yIndex) => {
    if (xBit != yBit && xIndex == yIndex) {
      distance++;
    }
  });
});
```

---

## Full Code

```javascript

const hammingDistance = function(x, y) {
  let xInBinary = x.toString(2);
  let yInBinary = y.toString(2);

  [xInBinary, yInBinary] = normalize(xInBinary, yInBinary);
  let distance = 0;

  xInBinary.split("").forEach((xBit, xIndex) => {
    yInBinary.split("").forEach((yBit, yIndex) => {
      if (xBit != yBit && xIndex == yIndex) {
        distance++;
      }
    });
  });

  return distance;
};

const normalize = function(x, y) {
  if (x.length < y.length) {
    for (let i = x.length; i < y.length; i++) {
      x = "0" + x;
    }
  } else {
    for (let i = y.length; i < x.length; i++) {
      y = "0" + y;
    }
  }
  return [x, y];
};

console.log(hammingDistance(1, 4));
```
